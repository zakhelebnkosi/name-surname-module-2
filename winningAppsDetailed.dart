void main() {
  var winningApp1 = new winningApp();

  winningApp1.name = "Ambani";
  winningApp1.category = "Education";
  winningApp1.developer = "Ambani Africa";
  winningApp1.year = 2021;

  winningApp1.printAppDetails();
  winningApp1.capitiliseAppName();
}

class winningApp {
  String? name;
  String? category;
  String? developer;
  int? year;

  void printAppDetails() {
    print("Name of App is: $name");
    print("Category: $category");
    print("Developed by: $developer");
    print("Year won: $year");
  }

  void capitiliseAppName() {
    print(name?.toUpperCase());
  }
}
